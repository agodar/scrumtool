package com.scrum.portal.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.scrum.portal.dto.Employee;

@Repository
public interface BKEmployeeRepository extends CrudRepository<Employee, Integer>
{
	
	

}
