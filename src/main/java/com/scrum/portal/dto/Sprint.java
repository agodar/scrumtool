package com.scrum.portal.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "sprint")
public class Sprint {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String name;
	private String description;
	private int releaseid;

	public int getReleaseid() { 
		return releaseid;
	}
	public void setReleaseid(int releaseid) {
		this.releaseid = releaseid;
	}
	public Sprint(){
		super();
	}
	public Sprint(String name,String description)
	{
		this.name = name;
		this.description = description;
	
	}
	
	@OneToOne
	@JoinColumn(name="id")
	private ReleaseBacklog releaseBacklog;
	
	public ReleaseBacklog getReleaseBacklog() {
		return releaseBacklog;
	}
	public void setReleaseBacklog(ReleaseBacklog releaseBacklog) {
		this.releaseBacklog = releaseBacklog;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}