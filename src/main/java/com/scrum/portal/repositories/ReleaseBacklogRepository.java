package com.scrum.portal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.scrum.portal.dto.ReleaseBacklog;


@Repository
public interface ReleaseBacklogRepository extends JpaRepository<ReleaseBacklog,Integer>{
	//@Query("SELECT P FROM RELEASEBACKLOG P WHERE P.PRODUCTID = :id")
	public List<ReleaseBacklog> findReleaseByProductId(@Param("pid") int pid) ;
	
}
