<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="span8">
		<div class="widget widget-nopad">
			
			<div class="widget widget-table action-table">
				<div class="widget-header">
					<c:if test="${userInfo.getRoleID()==7}"><a href="<c:url value="/releasebacklog/add/${pid}"/>"><i class="icon-plus-sign"></i>
					<h3>Add New Release</h3>
					</a></c:if>
				</div>
				<!-- /widget-header -->
				<c:if test="${flashSuccessMsg}">
				<div class="alert alert-success">${flashSuccessMsg}</div>
				</c:if>
				
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
									<c:if test="${userInfo.getRoleID()==7}"><th >Action</th></c:if>
								<c:if test="${userInfo.getRoleID()==3}"><th>Add/View Sprint</th></c:if>
								<th>Assign UStory</th>
							</tr>
						</thead>
						<tbody>
		<c:forEach items="${releasebacklogs}" var="releasebacklog">
							<tr>
								<td><c:out value="${releasebacklog.getName()}" /></td>
								<td><c:out value="${releasebacklog.getDescription()}" /></td>
								<c:if test="${userInfo.getRoleID()==7}">	<td class="td-actions"><a href="<c:url value="/releasebacklog/edit/${releasebacklog.getProductId()}/${releasebacklog.getId()}"/>"
									class="btn btn-small btn-success"><i
										class="btn-icon-only icon-ok"> </i></a><a href="<c:url value="/releasebacklog/delete/${releasebacklog.getProductId()}/${releasebacklog.getId()}"/>"
									class="btn btn-danger btn-small" id="deleteMe"><i
										class="btn-icon-only icon-remove"> </i></a></td></c:if>
										
								<c:if test="${userInfo.getRoleID()==3}"><td >
								<a href="<c:url value="/sprint/listsprints/${releasebacklog.getId()}"/>" class="btn  btn-success">  <i class="icon-large icon-eye-open"></i></a></td></c:if>
								<td ><a href="<c:url value="/userstory/assignuserstoriestorelease/release/${releasebacklog.getProductId()}/${releasebacklog.getId()}"/>" class="btn btn-danger" title="Assig User Story to Release">  <i class="icon-large icon-plus-sign"></i></a>  &nbsp;&nbsp;&nbsp;
								<a href="<c:url value="/sprint/listsprints/${releasebacklog.getId()}"/>" class="btn  btn-success">  <i class="icon-large icon-eye-open"></i></a></td>			
							</tr>
							
			</c:forEach>				
							
						</tbody>
					</table>
				</div>
				<!-- /widget-content -->
			</div>
		</div>
	</div>
	
	<c:if test="${userInfo.getRoleID()==7}">
	<div class="span4">
		<div class="widget ">

			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>${actionTitle}</h3>
			</div>
			<!-- /widget-header -->

			<div class="widget-content">


							<form id="add-sprint" class="form-vertical" method="POST"
								action="<spring:url value="${actionText}"/>"
								data-toggle="validator" role="form">
								<fieldset>

									<div class="control-group">
										<label  for="name">Title</label>
										<div class="controls">
											<input type="text" name="name" class="span3" id="name"
												value="${release.getName()} " required>
											<p class="help-block">Type the name of the Release .</p>
										</div>
										<!-- /controls -->
									</div>
									<!-- /control-group -->



									<div class="control-group">
										<label for="description">Description</label>
										<div class="controls">
											<textarea class="span3" id="description" name="description">${release.getDescription()} </textarea>
										</div>
										<!-- /controls -->
									</div>
									<!-- /control-group -->

									<div class="control-group">
										<label  for="name">Scrum Master</label>
										<div class="controls">
											<Select name="scrumMasterId" class="span3" id="scrumMasterId">
											
											<option value="0">Select Scrum Master</option>
											<c:forEach items="${scrumMasters}" var="scrumMaster">
												<option value="${scrumMaster.getId()}" <c:if test="${release.getScrumMasterId()==scrumMaster.getId()}"> selected </c:if>>${scrumMaster.getUserName()}</option>
											</c:forEach>	
											</Select>
											<p class="help-block">Choose the Scrum Master for this release .</p>
										</div>
										<!-- /controls -->
									</div>
									<br /> <br />

									<c:if test="${not empty productId}">
										<input type="hidden" value="${productId}" name="productId">
									</c:if>
									<c:if test="${not empty id}">
										<input type="hidden" value="${id}" name="Id   ">
									</c:if>
									<div class="form-actions">
										<button type="submit" class="btn btn-primary" name="submit">Save</button>
										<button class="btn">Cancel</button>
									</div>
									<!-- /form-actions -->
								</fieldset>
							</form>
						</div>

					</div>


				</div>


			<!-- /widget-content -->
</c:if>
		</div>
		<!-- /widget -->

