<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

      <div class="row">
	      	
	      	<div class="span12">
	      		
	      		<div class="widget">
						
					<div class="widget-header">
						<i class="icon-th-large"></i>
						<h3>Welcome : Scrum Master Current Products</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="pricing-plans plans-3">
				<c:forEach items="${listproduct}" var="product">	
					
						<div class="plan-container">
					        <div class="plan">
						        <div class="plan-header">
					                
						        	<div class="plan-title">
						        		<i class="shortcut-icon icon-file"></i> &nbsp;<c:out value="${product.getTitle()}" />	        		
					        		</div> <!-- /plan-title -->
					                
						           <!--  <div class="plan-price">
					                	5<span class="term">Releases</span>
									</div> /plan-price -->
									
						        </div> <!-- /plan-header -->	        
						        <div class="plan-actions">				
									<a href="<c:url value="/releasebacklog/listreleases/${product.getId()}"/>" class="btn"> <i class="shortcut-icon icon-list-alt"></i> &nbsp;&nbsp;View Release</a>				
								</div> 
								
								<!-- /plan-actions -->
								 <%-- <div class="plan-actions">				
									<a href="<c:url value="/userstory/listuserstories/product/${product.getId()}"/>" class="btn">Manage User Story</a>				
								</div> --%> <!-- /plan-actions -->
								<div class="plan-actions">				
									<a href="<c:url value="/userstory/assignuserstorytouser/developer/${product.getId()}"/>" class="btn"><i class="shortcut-icon icon-tag"></i> &nbsp;&nbsp;Assign User Story to Developer</a>				
								</div> <!-- /plan-actions -->
								<div class="plan-actions">				
									<a href="<c:url value="/userstory/assignuserstorytouser/tester/${product.getId()}"/>" class="btn"><i class="shortcut-icon icon-bookmark"></i> &nbsp;&nbsp;Assign User Story to Tester</a>				
								</div> <!-- /plan-actions -->
						       
								
								
					
							</div> <!-- /plan -->
					    </div> <!-- /plan-container -->
					 </c:forEach>   
					    
					    
					   
				
				
					</div> <!-- /pricing-plans -->
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->					
				
		    </div> <!-- /span12 -->     	
	      	
	      	
	      </div>